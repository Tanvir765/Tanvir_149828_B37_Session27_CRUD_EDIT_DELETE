<?php
namespace App\City;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class City extends DB{
    public $id = "";
    public $name = "";
    public $city_name = "";
    public function __construct()
    {
        parent::__construct();
    }
    public function setData($data=NULL){
        if(array_key_exists("id",$data)){
            $this->id=$data["id"];
        }
        if(array_key_exists("name",$data)){
            $this->name=$data["name"];
        }
        if(array_key_exists("city_name",$data)){
            $this->city_name=$data["city_name"];
        }
    }
    public function store(){
        $dbh=$this->connection;
        $values=array($this->name,$this->city_name);
        $query="insert into city(name,city_name) VALUES (?,?)";
        $sth=$dbh->prepare($query);
        $result= $sth->execute($values);

        if($result){
            Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ city_name: $this->city_name ] <br> Data Has Been Inserted Successfully!</h3></div>");
        }else{
            Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ city_name: $this->city_name ] <br> Data Has Not Been Inserted Successfully!</h3></div>");
        }

        Utility::redirect('create.php');


    }
}
