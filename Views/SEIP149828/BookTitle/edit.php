<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\BookTitle\BookTitle;

if(!isset( $_SESSION)) session_start();
echo "<div id =\"message\">". Message::message()."</div>";

$obj=new BookTitle();
$obj->setData($_GET);
$singleItem=$obj->view("obj");


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title</title>
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/booktitle.css" type="text/css">
</head>
<body>
<div class="container">
    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-heading">
                        <h1>Edit BookTile</h1>
                        <div class="row-fluid user-row">
                            <img src="../../../resource/images/book.jpg" class="img-responsive icon" alt="Conxole Admin"/>
                            <style>body{
                                    background-image:url("../../../resource/images/book_bag.jpg");
                                    background-repeat: repeat-x;
                                }</style>
                        </div>
                    </div>

                </div>
                <div class="panel-body">
                    <form accept-charset="UTF-8" role="form" class="form-booktitle" method="Post" action="update.php">

                        <fieldset>
                            <input type="hidden" value="<?php echo $singleItem->id?>" name="id">
                            <label class="panel-input">
                                <div class="input_result"></div>
                            </label>
                            <label class="" for="name">Edit Book Name</label>

                            <input class="form-control" value="<?php echo $singleItem->book_title?>" name="book_title" type="text">
                            <label class="" for="name">Edit Author Name</label>
                            <input class="form-control" value="<?php echo $singleItem->author_name?> " name="author_name" type="text">
                            </br>
                            <input class="btn btn-lg btn-success btn-block" type="submit" name="submit" value="Update">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>

<script>
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(1200).fadeOut();
</script>

