<?php
namespace App\BirthDate;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class BirthDate extends DB{
    public $id = "";
    public $name = "";
    public $birthday = "";
    public function __construct()
    {
        parent::__construct();
    }
    public function setData($data=NULL){
        if(array_key_exists("id",$data)){
            $this->id=$data["id"];
        }
        if(array_key_exists("name",$data)){
            $this->name=$data["name"];
        }
        if(array_key_exists("birthday",$data)){
            $this->birthday=$data["birthday"];
        }
    }
    public function store(){
        $dbh=$this->connection;
        $values=array($this->name,$this->birthday);
        $query="insert into birthday(name,birthday) VALUES (?,?)";
        $sth=$dbh->prepare($query);
        $result= $sth->execute($values);

        if($result){
            Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ Birthday: $this->birthday ] <br> Data Has Been Inserted Successfully!</h3></div>");
        }else{
            Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ Birthday: $this->birthday ] <br> Data Has Not Been Inserted Successfully!</h3></div>");
        }

        Utility::redirect('create.php');


    }
    public function index($Mode="ASSOC"){
        $Mode=strtoupper($Mode);

        $STH = $this->connection->query('SELECT * from birthday');


        if($Mode=="OBJ")   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;


    }
    public function view($fetchMode="ASSOC"){

        $STH = $this->connection->query('SELECT * from birthday where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode, "OBJ")>0)   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;

    }
    public function update(){

        $arrData  = array($this->name,$this->birthday);

        $sql = 'UPDATE book_title  SET book_title  = ?   , author_name = ? where id ='.$this->id;

        $STH = $this->connection->prepare($sql);


        $result = $STH->execute($arrData);

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Updated Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Updated Successfully!</h3></div>");

        Utility::redirect('index.php');

    }
    public function delete(){

        $sql = "DELETE FROM birthday WHERE id =".$this->id;

        $STH = $this->connection->prepare($sql);

        $result = $STH->execute();

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Deleted Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Deleted Successfully!</h3></div>");


        Utility::redirect('index.php');


    }
}

