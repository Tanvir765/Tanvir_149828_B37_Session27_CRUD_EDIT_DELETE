<html>
<head>
    <style>a{
            float: left;
        }</style>
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap-theme.min.css">
</head>
<body>
    <h1 align="center">Book Title Views</h1>
</body>
</html>




<?php
require_once("../../../vendor/autoload.php");
use App\BookTitle\BookTitle;

$obj = new BookTitle();
$allData = $obj->index("obj");


//Without OBJ:::::::::::::::::::::::::
/*echo"<table border='5px'>";
foreach ($allData as $oneData) {
    echo "<tr>";
    echo "<td>" . $oneData['id'] . "</td>";
    echo "<td>" . $oneData['book_title']. "</td>";
    echo "<td>" . $oneData['author_name']. "</td>";
    echo "</tr>";
}
echo"</table>";
*/

//with OBJ::::::::::::::::::::::::::

echo"<table border='5px' style='width: 50%; margin: auto'>";

echo"<th>Serial</th>";
echo"<th>Id</th>";
echo"<th>Book Name</th>";
echo"<th>Author Name</th>";
echo"<th>Action</th>";

$serial=0;
foreach ($allData as $oneData) {
    echo "<tr>";
    echo "<td>" .++ $serial. "</td>";
    echo "<td>" . $oneData->id . "</td>";
    echo "<td>" . $oneData->book_title. "</td>";
    echo "<td>" . $oneData->author_name. "</td>";
    echo "<td> <a href=\"edit.php?id=$oneData->id\"  class=\"btn btn-lg btn-success\" role=\"button\">Edit</a> ";
    echo " <a href=\"delete.php?id=$oneData->id\"  class=\"btn btn-lg btn-danger\" role=\"button\">Delete</a> </td> ";
    echo "</tr>";
}
echo"</table>";

?>


