<html>
<head>
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap-theme.min.css">
</head>
</html>

<?php

require_once("../../../vendor/autoload.php");
use App\BirthDate\BirthDate;

$obj=new BirthDate();

$allData = $obj->index("obj");

echo"<table border='5px' style='width: 30%; margin: auto'>";
foreach ($allData as $oneData) {
    echo "<tr>";
    echo "<td>" . $oneData->id . "</td>";
    echo "<td>" . $oneData->name. "</td>";
    echo "<td>" . $oneData->birthday. "</td>";
    echo "<td> <a href=\"edit.php?id=$oneData->id\"  class=\"btn btn-lg btn-success\" role=\"button\">Edit</a> </td> ";
    echo "<td> <a href=\"delete.php?id=$oneData->id\"  class=\"btn btn-lg btn-danger\" role=\"button\">Delete</a> </td> ";
    echo "</tr>";
}
echo"</table>";
?>