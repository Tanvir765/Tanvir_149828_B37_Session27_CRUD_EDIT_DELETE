<?php
namespace App\BookTitle;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;

class BookTitle extends DB{
    public $id = "";
    public $book_title = "";
    public $author_name = "";
    public function __construct()
    {
        parent::__construct();
    }
    public function setData($data=NULL){
        if(array_key_exists("id",$data)){
            $this->id=$data["id"];
        }
        if(array_key_exists("book_title",$data)){
            $this->book_title=$data["book_title"];
        }
        if(array_key_exists("author_name",$data)){
            $this->author_name=$data["author_name"];
        }
    }
    public function store(){
        $dbh=$this->connection;
        $values=array($this->book_title,$this->author_name);
        $query="insert into book_title(book_title,author_name) VALUES (?,?)";
        $sth=$dbh->prepare($query);
       $result= $sth->execute($values);

        if($result){
            Message::message("<div id='msg'></div><h3 align='center'>[ BookTitle: $this->book_title ] , [ AuthorName: $this->author_name ] <br> Data Has Been Inserted Successfully!</h3></div>");
        }else{
            Message::message("<div id='msg'></div><h3 align='center'>[ BookTitle: $this->book_title ] , [ AuthorName: $this->author_name ] <br> NO Data Has Been Inserted!!!!!!</h3></div>");

        }
        Utility::redirect('create.php');
    }
    public function index($Mode="ASSOC"){
        $Mode=strtoupper($Mode);

        $STH = $this->connection->query('SELECT * from book_title');


        if($Mode=="OBJ")   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;


    }
    public function view($fetchMode="ASSOC"){

        $STH = $this->connection->query('SELECT * from book_title where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode, "OBJ")>0)   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;

    }
    public function update(){

        $arrData  = array($this->book_title,$this->author_name);

        $sql = 'UPDATE book_title  SET book_title  = ?   , author_name = ? where id ='.$this->id;

        $STH = $this->connection->prepare($sql);


        $result = $STH->execute($arrData);

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Updated Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Updated Successfully!</h3></div>");

        Utility::redirect('index.php');

    }
    public function delete(){

        $sql = "DELETE FROM book_title WHERE id =".$this->id;

        $STH = $this->connection->prepare($sql);

        $result = $STH->execute();

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Deleted Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Deleted Successfully!</h3></div>");


        Utility::redirect('index.php');


    }
}
